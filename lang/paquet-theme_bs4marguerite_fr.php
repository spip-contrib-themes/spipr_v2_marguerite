<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_marguerite
// Langue: fr
// Date: 25-03-2020 17:09:16
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4marguerite_description' => 'Du vert et du jaune-orange.',
	'theme_bs4marguerite_slogan' => 'Du vert et du jaune-orange',
);
?>